#ifndef TESTINN_H
#define TESTINN_H

#include <QString>

// класс проврки инн и снилс

class testinn
{
public:
    testinn();
    ~testinn();
    bool testinns(const QString &);
    bool testsnils(const QString &);
};

#endif // TESTINN_H
