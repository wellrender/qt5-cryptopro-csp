#include "dialog.h"
#include "ui_dialog.h"
#include <QtCore/QProcess>
#include <QtCore/QTextStream>
#include <QMessageBox>
#include <QFileDialog>

//Окно управления сертификатом пользователя

Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);
}

Dialog::~Dialog()
{
    delete ui;
}

void Dialog::on_pushButton_quit_clicked()
{
  close();
}

// certmgr -inst -ask-cont -at_signature -cont cont -pin pin

void Dialog::on_pushButton_inst_clicked()
{
    QProcess certmgr;
    QString conteiner;
    if(checked){
        QTextStream (&conteiner) << pathcr << " -inst" <<  " -cont " << cont << " -store uMy";
    }
    else {
        QTextStream (&conteiner) << pathcr << " -inst" << " -ask-cont " << "-at_signature" << " -cont " << cont << " -store uMy";
    }

    certmgr.setProcessChannelMode(QProcess::MergedChannels);
    certmgr.start(conteiner);
    if (!certmgr.waitForFinished()){
        QMessageBox mbox;
        mbox.setText(certmgr.errorString());
        mbox.exec();
        certmgr.close();
    }
    else{
        QString out;
        QMessageBox mbox;
        out.append(certmgr.readAllStandardOutput());
        out.append(certmgr.readAllStandardOutput());
        mbox.setText(out);
        mbox.exec();
        certmgr.close();
        }
    certmgr.close();
}

void Dialog::labelText(){
    //наименование контейнера
    //certmgr -list -ask-cont -at_signature -cont
    QProcess certmgr;
    QString text;
    QString conteiner;
    if(checked){
        QTextStream(&conteiner) << pathcr << " -list" <<  " -cont " << cont;
    }
    else {
        QTextStream(&conteiner) << pathcr << " -list" << " -ask-cont" << " -at_signature" << " -cont " << cont;
    }
    certmgr.setProcessChannelMode(QProcess::MergedChannels);
    //certmgr.start(pathcr, arguments << "-list" << "-ask-cont" << "" << "-cont" << "\'\\\\.\\HDIMAGE\\test\'");
    certmgr.start(conteiner);
    if (!certmgr.waitForFinished()){
         ui->label_sert->setText(certmgr.errorString());
    }
    else{
        QString outtext = tr(certmgr.readAllStandardOutput());
        QStringList outtextlist = outtext.split("\n");
        foreach (QString t, outtextlist) {
            if (t.operator !=("============================================================================="))
                QTextStream (&text) << t << "\n";
            if (t.operator ==("[ErrorCode: 0x8010002c]"))
                QTextStream(&text) << tr("\nСертификат в контейнере отсутствует") << "\n";
            if (t.operator ==("[ErrorCode: 0x8009001f]"))
                QTextStream(&text) << tr("Контейнер не обноружен") << "\n";
            if (t.operator ==("[ErrorCode: 0x8009000d]"))
                QTextStream(&text) << tr("\nПоставте флажок тип 2") << "\n";
            if (t.startsWith("Subject")){
                subject = t;
            }
        }
        ui->label_sert->setText(text);
        outtextlist.clear();
    }
    certmgr.close();
}

void Dialog::on_pushButton_export_clicked()
{
    //cryptcp -copycert -dn E=user@test.ru -df personal.cer
    QFileDialog * dialog = new QFileDialog();
    QString namefile = dialog->getExistingDirectory(this,"Выберите каталог для сохранения сертификата","/home");
    delete dialog;

    if (namefile.length() != 0){

        QStringList subj = subject.split(": ");
        QString sub = subj[1];
        subj.clear();
        subj = sub.split(", ");
        QString sub1 = subj[0];
        subj.clear();
        subj = sub1.split("=");
        QString name = subj[1];
        QProcess certmgr;
        QString crcp;
        QTextStream(&crcp) << pathcr << " -export -cert -dn " << sub1 << " -dest \"" << namefile << "/" << name << ".cer\"";
        //qDebug() << crcp;
        certmgr.setProcessChannelMode(QProcess::MergedChannels);
        certmgr.start(crcp);
        if (!certmgr.waitForFinished()){
            QMessageBox mbox;
            mbox.setText(certmgr.errorString());
            mbox.exec();
            certmgr.close();
        }
        else{
            QString out;
            QMessageBox mbox;
            out.append(certmgr.readAllStandardOutput());
            out.append(certmgr.readAllStandardOutput());
            mbox.setText(out);
            mbox.exec();
            certmgr.close();
            }
        certmgr.close();
        subj.clear();
    }
}
