﻿#include "applydoc.h"
#include <QtCore/QFile>
#include <QtCore/QDir>
#include <QtCore/QTextStream>
#include <QTextTableFormat>
#include <QTextDocumentWriter>


//Класс генерации документа заявки на сертификат в формате odt

applyDoc::applyDoc()
    :m_document(new QTextDocument()),
    m_cursor(m_document)
{
    QString dir = QDir::homePath();
    QTextStream(&dir) << "/.qt5-cryptopro-csp/polname.dat";
    QFile file (dir);
    file.open(QIODevice::ReadOnly);
    QDataStream in(&file);

    in >> map;

}

 void applyDoc::shapeapply()
 {
     QString nameuser;

     QTextStream(&nameuser) << map.value("SN") << " " << map.value("G");

     QTextBlockFormat blockformat;
     blockformat.setAlignment(Qt::AlignCenter);
     m_cursor.setBlockFormat(blockformat);
     QTextCharFormat textformat;
     textformat.setFontFamily("Times New Roman");
     textformat.setFontWeight(100);
     m_cursor.movePosition(QTextCursor::End);
     m_cursor.insertText(QObject::tr("Заявление на изготовление Квалифицированного Сертификата\n"), textformat);
     m_cursor.movePosition(QTextCursor::End);
     blockformat.setAlignment(Qt::AlignJustify);
     blockformat.setTextIndent(30);
     m_cursor.setBlockFormat(blockformat);
     textformat.setFontWeight(50);
     m_cursor.insertText("\nПросим создать ключ электронной подписи, ключ проверки электронной подписи, "
                         "изготовить квалифицированный сертификат ключа проверки электронной подписи в "
                         "соответствии с указанными в настоящем заявлении данными:\n", textformat);
     QBrush  borderBrush(Qt::SolidPattern);
     QTextTableFormat tableFormat;
     tableFormat.setCellPadding(5);
     tableFormat.setCellSpacing(0);
     tableFormat.setHeaderRowCount(1);
     tableFormat.setBorderBrush(borderBrush);
     tableFormat.setBorderStyle(QTextFrameFormat::BorderStyle_Solid);
     tableFormat.setBorder(1);
     tableFormat.setWidth(QTextLength(QTextLength::PercentageLength, 100));
     m_cursor.insertTable(17, 2, tableFormat);
     m_cursor.insertText(QObject::tr("Фамилия Имя Отчество (CN)"), textformat);
     m_cursor.movePosition(QTextCursor::NextCell);
     m_cursor.insertText(nameuser, textformat);
     m_cursor.movePosition(QTextCursor::NextRow);
     m_cursor.insertText(QObject::tr("Фамилия (SN)"), textformat);
     m_cursor.movePosition(QTextCursor::NextCell);
     m_cursor.insertText(map.value("SN"), textformat);
     m_cursor.movePosition(QTextCursor::NextRow);
     m_cursor.insertText(QObject::tr("Имя и Отчество (G)"), textformat);
     m_cursor.movePosition(QTextCursor::NextCell);
     m_cursor.insertText(map.value("G"), textformat);
     m_cursor.movePosition(QTextCursor::NextRow);
     m_cursor.insertText(QObject::tr("СНИЛС (SNILS)"), textformat);
     m_cursor.movePosition(QTextCursor::NextCell);
     m_cursor.insertText(map.value("SNILS"), textformat);
     m_cursor.movePosition(QTextCursor::NextRow);
     m_cursor.insertText(QObject::tr("Должность (T)"), textformat);
     m_cursor.movePosition(QTextCursor::NextCell);
     m_cursor.insertText(map.value("T"), textformat);
     m_cursor.movePosition(QTextCursor::NextRow);
     m_cursor.insertText(QObject::tr("Наименование организации (O)"), textformat);
     m_cursor.movePosition(QTextCursor::NextCell);
     m_cursor.insertText(map.value("O"), textformat);
     m_cursor.movePosition(QTextCursor::NextRow);
     m_cursor.insertText(QObject::tr("Наименование подразделения"), textformat);
     m_cursor.movePosition(QTextCursor::NextCell);
     m_cursor.insertText(map.value("OU"), textformat);
     m_cursor.movePosition(QTextCursor::NextRow);
     m_cursor.insertText(QObject::tr("Тип Участника"), textformat);
     m_cursor.movePosition(QTextCursor::NextRow);
     m_cursor.insertText(QObject::tr("Полномочия"), textformat);
     m_cursor.movePosition(QTextCursor::NextRow);
     m_cursor.insertText(QObject::tr("Улица, Дом, Корпус, Помещение(Street adress)"), textformat);
     m_cursor.movePosition(QTextCursor::NextCell);
     m_cursor.insertText(map.value("STREET"), textformat);
     m_cursor.movePosition(QTextCursor::NextRow);
     m_cursor.insertText(QObject::tr("Город (L)"), textformat);
     m_cursor.movePosition(QTextCursor::NextCell);
     m_cursor.insertText(map.value("L"), textformat);
     m_cursor.movePosition(QTextCursor::NextRow);
     m_cursor.insertText(QObject::tr("Область (S)"), textformat);
     m_cursor.movePosition(QTextCursor::NextCell);
     m_cursor.insertText(QObject::tr("64 Саратовская область"), textformat);
     m_cursor.movePosition(QTextCursor::NextRow);
     m_cursor.insertText(QObject::tr("Страна (С)"), textformat);
     m_cursor.movePosition(QTextCursor::NextCell);
     m_cursor.insertText(QObject::tr("RU"), textformat);
     m_cursor.movePosition(QTextCursor::NextRow);
     m_cursor.insertText(QObject::tr("Адрес электронной почты (E)"), textformat);
     m_cursor.movePosition(QTextCursor::NextCell);
     m_cursor.insertText(map.value("E"), textformat);
     m_cursor.movePosition(QTextCursor::NextRow);
     m_cursor.insertText(QObject::tr("ИНН (INN)"), textformat);
     m_cursor.movePosition(QTextCursor::NextCell);
     m_cursor.insertText(map.value("INN"), textformat);
     m_cursor.movePosition(QTextCursor::NextRow);
     m_cursor.insertText(QObject::tr("ОГРН (OGRN)"), textformat);
     m_cursor.movePosition(QTextCursor::NextCell);
     m_cursor.insertText(map.value("OGRN"), textformat);
     m_cursor.movePosition(QTextCursor::NextRow);
     m_cursor.insertText(QObject::tr("Неструктурированное имя"), textformat);
     m_cursor.movePosition(QTextCursor::NextCell);
     m_cursor.insertText(map.value("OID.1.2.840.113549.1.9.2"), textformat);
     m_cursor.movePosition(QTextCursor::End);
     blockformat.setTextIndent(0);
     m_cursor.setBlockFormat(blockformat);
     QString block1;
     QTextStream(&block1) << QObject::tr("\nНастоящим ") << map.value("CN") << QObject::tr(" паспорт, серия и номер:\t") << map.value("passportser") << "\t"
                         << map.value("passport") << QObject::tr(" дата выдачи: ") << map.value("data") << QObject::tr(" кем выдан: ") << map.value("issued")
                         << QObject::tr (" соглашается с обработкой своих персональных данных Удостоверяющим центром и признает, что персональные данные, "
                         "заносимые ИСПОЛНИТЕЛЕМ в квалифицированный Сертификат, владельцем которого он является, относятся к общедоступным персональным данным.\n"
                         "\nУполномоченный представитель\t\t\t\t") << map.value("authorized") << "\n";
     m_cursor.insertText(block1, textformat);
     m_cursor.movePosition(QTextCursor::End);
     blockformat.setTextIndent(300);
     m_cursor.setBlockFormat(blockformat);
     textformat.setFontPointSize(8);
     m_cursor.insertText("(подпись)\t\t(расшифровка подписи)\n", textformat);
     m_cursor.movePosition(QTextCursor::End);
     blockformat.setTextIndent(0);
     m_cursor.setBlockFormat(blockformat);
     textformat.setFontPointSize(12);
     m_cursor.insertText("\nНастоящим ЗАКАЗЧИК соглашается, что ИСПОЛНИТЕЛЬ не несет ответственности за действия владельцев информационных систем, приведших "
                         "к невозможности использования квалифицированных сертификатов в этих информационных системах."
                         "\nНастоящим ЗАКАЗЧИК присоединяется к Регламенту Удостоверяющего центра, опубликованном на сайте Удостоверяющего центра, "
                         "по адресу: http://sarmiac.medportal.saratov.gov.ru/udostovcent/.\n\nЗАКАЗЧИК\n", textformat);
     m_cursor.movePosition(QTextCursor::End);
     m_cursor.setBlockFormat(blockformat);
     QString block2;
     QTextStream(&block2) << "\n" << map.value("post") << "\t\t\t\t\t" << map.value("boss") << "\n";
     m_cursor.insertText(block2, textformat);
     m_cursor.movePosition(QTextCursor::End);
     textformat.setFontPointSize(8);
     m_cursor.setBlockFormat(blockformat);
     m_cursor.insertText("(должность руководителя)\t\t\t(подпись)\t\t(расшифровка подписи)\n", textformat);
     m_cursor.movePosition(QTextCursor::End);
     textformat.setFontPointSize(12);
     m_cursor.insertText("\n\tМП\n", textformat);
     m_cursor.movePosition(QTextCursor::End);
 }
 void applyDoc::shapetrast(){
     QString nameuser;
     QTextStream(&nameuser) << map.value("SN") << " " << map.value("G");

     QTextBlockFormat blockformat;
     blockformat.setAlignment(Qt::AlignCenter);
     m_cursor.setBlockFormat(blockformat);
     QTextCharFormat textformat;
     textformat.setFontFamily("Times New Roman");
     textformat.setFontWeight(100);
     m_cursor.movePosition(QTextCursor::End);
     m_cursor.insertText(QObject::tr("ДОВЕРЕННОСТЬ № ________\n\n"), textformat);
     m_cursor.movePosition(QTextCursor::End);
     blockformat.setAlignment(Qt::AlignJustify);
     blockformat.setTextIndent(300);
     m_cursor.setBlockFormat(blockformat);
     textformat.setFontWeight(50);
     m_cursor.insertText(QObject::tr("Дата выдачи\t\"____\" __________ 20___г.\nДействительна по\t\"____\" __________ 20___г.\n\n"), textformat);
     m_cursor.movePosition(QTextCursor::End);
     blockformat.setTextIndent(0);
     m_cursor.setBlockFormat(blockformat);
     QString block1;
     QTextStream(&block1) << QObject::tr("Я, ") << nameuser << "\n";
     textformat.setFontWeight(100);
     m_cursor.insertText(block1, textformat);
     m_cursor.movePosition(QTextCursor::End);
     blockformat.setAlignment(Qt::AlignCenter);
     m_cursor.setBlockFormat(blockformat);
     textformat.setFontWeight(50);
     textformat.setFontPointSize(8);
     m_cursor.insertText("(фамилия, имя, отчество Уполномоченного, ответственного лица)\n", textformat);
     m_cursor.movePosition(QTextCursor::End);
     QString block2;
     QTextStream(&block2) << map.value("T") << " " << map.value("O") << "\n";
     blockformat.setAlignment(Qt::AlignJustify);
     m_cursor.setBlockFormat(blockformat);
     textformat.setFontWeight(50);
     textformat.setFontPointSize(12);
     m_cursor.insertText(block2, textformat);
     m_cursor.movePosition(QTextCursor::End);
     blockformat.setAlignment(Qt::AlignCenter);
     m_cursor.setBlockFormat(blockformat);
     textformat.setFontPointSize(8);
     m_cursor.insertText("(должность название организации)\n", textformat);
     m_cursor.movePosition(QTextCursor::End);
     blockformat.setAlignment(Qt::AlignJustify);
     m_cursor.setBlockFormat(blockformat);
     textformat.setFontWeight(50);
     textformat.setFontPointSize(12);
     QString start;
     QTextStream(&start) << QObject::tr("Паспорт\t") << map.value("passportser") << "\t" << map.value("passport")
                         << QObject::tr("\n\tКем выдан:\t") << map.value("issued") << QObject::tr("\n\tДата выдачи:\t") << map.value("data") << "\n";
     m_cursor.insertText(start, textformat);
     m_cursor.movePosition(QTextCursor::End);
     blockformat.setAlignment(Qt::AlignJustify);
     m_cursor.setBlockFormat(blockformat);
     textformat.setFontWeight(100);
     textformat.setFontPointSize(12);
     QString mytext;
     QTextStream(&mytext) << QObject::tr("ДОВЕРЯЮ ") << map.value("authorfull") << " " << map.value("authorT") << " " << map.value("O") << "\n";
     m_cursor.insertText(mytext, textformat);
     m_cursor.movePosition(QTextCursor::End);
     m_cursor.setBlockFormat(blockformat);
     textformat.setFontWeight(50);
     textformat.setFontPointSize(8);
     m_cursor.insertText("\t(фамилия, имя, отчество, должность, название организации)\n", textformat);
     m_cursor.movePosition(QTextCursor::End);
     blockformat.setAlignment(Qt::AlignJustify);
     m_cursor.setBlockFormat(blockformat);
     textformat.setFontPointSize(12);
     QString block22;
     QTextStream(&block22) << QObject::tr("Паспорт\t") << map.value("authorPs") << "\t" << map.value("authorP")
                         << QObject::tr("\n\tКем выдан:\t") << map.value("authorIs") << QObject::tr("\n\tДата выдачи:\t") << map.value("authorD") << "\n";
     m_cursor.insertText(block22, textformat);
     m_cursor.movePosition(QTextCursor::End);
     textformat.setFontWeight(100);
     textformat.setFontPointSize(12);
     m_cursor.insertText("ВЫПОЛНИТЬ СЛЕДУЮЩЕЕ:\n", textformat);
     m_cursor.movePosition(QTextCursor::End);
     blockformat.setTextIndent(32);
     m_cursor.setBlockFormat(blockformat);
     textformat.setFontWeight(50);
     textformat.setFontPointSize(12);
     m_cursor.insertText("вместо меня присутствовать при создании моего сертификата ключа проверки электронной подписи (ЭП);\n"
                         "получить ключевой носитель, содержащий:\n"
                         "\t- ключевые файлы в контейнере;\n"
                         "\t- сертификат ключа проверки ЭП;\n"
                         "получить мой сертификат ключа проверки ЭП;\n"
                         "расписаться за меня в сертификате ключа проверки ЭП  и актах;\n"
                         "расписаться за меня в журнале поэкземплярного учёта СКЗИ.\n", textformat);
     m_cursor.movePosition(QTextCursor::End);
     blockformat.setTextIndent(32);
     m_cursor.setBlockFormat(blockformat);
     textformat.setFontWeight(100);
     textformat.setFontPointSize(12);
     m_cursor.insertText("Без права передоверия.\n\n\n", textformat);
     m_cursor.movePosition(QTextCursor::End);
     blockformat.setTextIndent(0);
     m_cursor.setBlockFormat(blockformat);
     textformat.setFontWeight(50);
     textformat.setFontPointSize(12);
     QString block3;
     QTextStream(&block3) << QObject::tr("Подпись лица, получившего доверенность _____________________\t") << map.value("authorized");
     m_cursor.insertText(block3, textformat);
     m_cursor.movePosition(QTextCursor::End);
     m_cursor.setBlockFormat(blockformat);
     textformat.setFontPointSize(8);
     m_cursor.insertText("\n\t\t\t\t\t(подпись)\t\t(Фамилия, И.О.)\n", textformat);
     m_cursor.movePosition(QTextCursor::End);
     QString block4;
     QTextStream(&block4) << QObject::tr("\nПодпись лица, выдавшего доверенность\t_____________________\t") << map.value("SN") << " " << map.value("I");
     m_cursor.setBlockFormat(blockformat);
     textformat.setFontWeight(50);
     textformat.setFontPointSize(12);
     m_cursor.insertText(block4, textformat);
     m_cursor.movePosition(QTextCursor::End);
     m_cursor.setBlockFormat(blockformat);
     textformat.setFontPointSize(8);
     m_cursor.insertText("\n\t\t\t\t\t(подпись)\t\t(Фамилия, И.О.)\n", textformat);
     m_cursor.movePosition(QTextCursor::End);
     QString block5;
     QTextStream(&block5) << QObject::tr("\nУДОСТОВЕРЯЮ\n\n") << map.value("post") << " " << map.value("O")
                          << QObject::tr("\t\t___________________\t") << map.value("boss");
     textformat.setFontWeight(50);
     textformat.setFontPointSize(12);
     m_cursor.setBlockFormat(blockformat);
     m_cursor.insertText(block5, textformat);
     m_cursor.movePosition(QTextCursor::End);
     m_cursor.setBlockFormat(blockformat);
     textformat.setFontPointSize(8);
     m_cursor.insertText("\n(должность руководителя, название организации)\t\t\t(подпись)\t\t(Фамилия, И.О.)\n", textformat);
     m_cursor.movePosition(QTextCursor::End);
     textformat.setFontPointSize(12);
     m_cursor.setBlockFormat(blockformat);
     m_cursor.insertText("\n\n\t\t\t\t\tМП\n\n\t\t\t\t\"______\"__________________ 20___г.\n", textformat);
     m_cursor.movePosition(QTextCursor::End);
 }

applyDoc::~applyDoc()
{
    delete m_document;
}

void applyDoc::write(const QString &fileName)
{
    QTextDocumentWriter writer(fileName);
    writer.write(m_document);
}
