#-------------------------------------------------
#
# Project created by QtCreator 2016-10-15T16:15:02
#
#-------------------------------------------------

QT       += core gui \
        sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Qt5-cryptopro
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    mytranslit.cpp \
    dialog.cpp \
    applydoc.cpp \
    instalgui.cpp \
    testinn.cpp

HEADERS  += mainwindow.h \
    mytranslit.h \
    dialog.h \
    applydoc.h \
    instalgui.h \
    testinn.h

FORMS    += mainwindow.ui \
    dialog.ui \
    instalgui.ui

DISTFILES += \
    screepts/cpconfig.sh \
    screepts/update.sh \
    screepts/updatea7.sh \
    screepts/updatea8.sh \
    screepts/createarch.sh \
    screepts/installcpcsp.sh \
    screepts/installpak.sh \
    screepts/installpaka.sh \
    screepts/installcades.sh \
    screepts/installcadesa.sh \
    screepts/updated78.sh

OTHER_FILES +=
