#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "mytranslit.h"
#include "dialog.h"
#include "applydoc.h"
#include "instalgui.h"
#include "testinn.h"

#include <QtCore/QFile>
#include <QtCore/QDataStream>
#include <QtCore/QTextStream>
#include <QtCore/QIODevice>
#include <QtCore/QProcess>
#include <QMessageBox>
#include <QFileDialog>
#include <QApplication>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    data_Read();

    //Свдения о лицензии
    // cpconfig -license -view

    if (TestCryptopro()){
        QString cpconfigPath = scryptPath;
        QTextStream(&cpconfigPath) << tr("cpconfig");
        QString license = LicenseVerific(cpconfigPath);
        QString out;
        QTextStream(&out) << tr("ДА ") << license;
        ui->label_cryptopro->setText(out);
        ui->label_cryptopro->setStyleSheet("color: green");
        //ui->pushButton_cryptopro->setDisabled(true);
    }
    else{
        ui->label_cryptopro->setText("НЕТ");
        ui->label_cryptopro->setStyleSheet("color: red");
        ui->gridLayout_2->setEnabled(false);
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_quit_clicked()
{
    date_Set();
    close();
}

// обработчик исключений
bool MainWindow::ErrorsInspection(QString &errors)
{
    bool noerrors = true;
    testinn * testErrorInn = new testinn();

    if (ui->lineEdit_SN->text().length() == 0){
        QTextStream(&errors) << tr("Нет фамилии (SN)!\n");
        noerrors = false;
    }
    if (ui->lineEdit_G->text().length() == 0){
        QTextStream(&errors) << tr("Нет имени и отчества (G)!\n");
        noerrors = false;
    }
    if (ui->lineEdit_I->text().length() == 0){
        QTextStream(&errors) << tr("Нет инициаллов (I)!\n");
        noerrors = false;
    }

    if (ui->lineEdit_T->text().length() == 0){
        QTextStream(&errors) << tr("Нет должности (T)!\n");
        noerrors = false;
    }

    if (ui->lineEdit_O->text().length() == 0){
        QTextStream(&errors) << tr("Нет орнанизации (O)!\n");
        noerrors = false;
    }

    if (ui->lineEdit_Street->text().length() == 0){
        QTextStream(&errors) << tr("Нет адреса (STREET)!\n");
        noerrors = false;
    }
    QString E = ui->lineEdit_E->text();

    if (E.length() == 0){
        QTextStream(&errors) << tr("Нет email (E)!\n");
        noerrors = false;
    }
    bool e = true;

    for (int i = 0; i < E.size(); ++i)
    {
        if (E.at(i) == QChar('@'))
            e = false;
    }

    if (e){
        QTextStream(&errors) << tr("Некорректный email (E)!\n");
        noerrors = false;
    }

    QString inn = ui->lineEdit_Snils->text();

    if (inn.length() < 11 || !testErrorInn->testsnils(inn)){
        QTextStream(&errors) << tr("Некорректный намер пенсионного страхования (SNILS)!\n");
        noerrors = false;
    }

    inn = ui->lineEdit_INN->text();

    if (inn.length() < 12 || !testErrorInn->testinns(inn)){
        QTextStream(&errors) << tr("Некорректный ИНН (INN)!\n");
        noerrors = false;
    }

    if (ui->lineEdit_OGRN->text().length() < 13){
        QTextStream(&errors) << tr("Некорректный ОГРН (OGRN)!\n");
        noerrors = false;
    }

    if (ui->lineEdit_L->text().length() == 0){
        QTextStream(&errors) << tr("Нет города (L)!\n");
        noerrors = false;
    }

    if (!ui->checkBox_pgu->isChecked() && !ui->checkBox_egis->isChecked()){
        QTextStream(&errors) << tr("Не выбраны политики сертификата!\n");
        return false;
    }

    if (!ui->checkBox_makekey->isChecked() && ui->comboBox_seakeys->currentText() == "none")
    {
       QTextStream(&errors) << tr("Нет доступныйх контейнеров!\n");
       return false;
    }
    delete testErrorInn;
    return noerrors;
}

// генератор запросов на сертификат
void MainWindow::on_pushButton_req_clicked()
{

    QFile filelog ("/tmp/genreg.log");
    filelog.open(QIODevice::WriteOnly);
    QTextStream out(&filelog);

    //Проверяем введенные данные
    QString errors = "\n";
    bool err = ErrorsInspection(errors);
    if (err){
        //Записываем в базу
        date_Set();

        //Собираем параметры политик имен
        QString options;

        QStringList keyss;
        keyss << "SN" << "G" << "I" << "CN" << "OID.1.2.840.113549.1.9.2"
              << "T" << "OU" << "E" << "O" << "STREET" << "SNILS" << "INN" << "OGRN" << "L";

        foreach (QString key, keyss) {
            QString value = map.value(key);
            if (value != "")
            {
                QTextStream(&options) << key << "=" << value << ",";
            }
        }

        QTextStream(&options) << "C=" << ui->comboBox_C->currentText() << ","
                              << "S=" << ui->comboBox_S->currentText();

        // Формируем политики сертификата

        QString boxcheck = "\"";

        if (ui->checkBox_egis->isChecked() && !ui->checkBox_pgu->isChecked()){
            QTextStream(&boxcheck) << "1.2.643.100.114.2,1.2.643.100.113.1,1.2.643.100.113.2";
        }
        if (ui->checkBox_pgu->isChecked() && !ui->checkBox_egis->isChecked()){
            QTextStream(&boxcheck) << "1.2.643.100.114.2,1.2.643.100.113.1,1.2.643.100.113.2";
        }

        if (ui->checkBox_pgu->isChecked() && ui->checkBox_egis->isChecked()) {
            QTextStream(&boxcheck) << "1.2.643.100.114.2,1.2.643.100.113.1,1.2.643.100.113.2";
        }

        if (ui->checkBox_aunt->isChecked()){
            QTextStream(&boxcheck) << ",1.3.6.1.5.5.7.3.2";
        }

        if (ui->checkBox_write->isChecked()){
            QTextStream(&boxcheck) << ",1.3.6.1.4.1.311.10.3.12";
        }

        QTextStream(&boxcheck) << "\"";

        //Выбор контейнера

        QString keys;

        QString n;
        QString c = ui->lineEdit_CN->text();
        mytranslit translit;
        translit.myTranslit(c, n);

        if (ui->checkBox_makekey->isChecked()){
            if (ui->comboBox_fd->currentText() == "FLASH"){
                keys.clear();
                QTextStream(&keys)  << "-cont \\\\.\\FLASH\\" << n;
            }
            else if (ui->comboBox_fd->currentText() == "RTOKEN"){
                keys.clear();
                QTextStream(&keys) << "-cont \\\\.\\Aktiv Co. Rutoken S 00 00\\" << n;
            }
            else {
                keys.clear();
                QTextStream(&keys) << "-cont \\\\.\\HDIMAGE\\" << n;
            }
        }
        else {
            keys = "-nokeygen -cont ";
            QTextStream(&keys) << ui->comboBox_seakeys->currentText();
        }

        //Фаил для сохранения
        QFileDialog * dialog = new QFileDialog();
        QString namefile = dialog->getExistingDirectory(this,"Выберите директорию для сохранения файла",homedir);

        delete dialog;

        if(namefile.length() != 0){

            QTextStream(&namefile) << "/" << n << ".req";

            QFile file(namefile);

            if(file.open(QIODevice::WriteOnly)){
                file.close();

                //Собираем все пораметры в список
                QString arguments;
                QTextStream(&arguments) << cryptPath << "cryptcp " << "-creatrqst " << "-dn \"" << options << "\" -provtype" << " 75" << " -both " << keys
                          << " -certusage " << boxcheck << " -exprt " << "\"" << namefile << "\"";

                QProcess cryptcp;
                cryptcp.setProcessChannelMode(QProcess::MergedChannels);
                cryptcp.start(arguments);
                if (!cryptcp.waitForFinished()){
                    QString errorlog = cryptcp.errorString();
                    out << errorlog;
                    QMessageBox mbox;
                    mbox.setText(errorlog);
                    mbox.exec();
                    cryptcp.close();
                }
                else{
                    QMessageBox mbox;
                    QString read = cryptcp.readAll();
                    QTextStream(&read) << tr("\nФаил запроса: ") << namefile << "\n";
                    out << read;
                    mbox.setText(read);
                    mbox.exec();
                    cryptcp.close();
                }
                cryptcp.close();
                filelog.close();
             }
            else{
                QMessageBox mbox;
                mbox.setText("Выберите другую папку для сохранения запроса");
                mbox.exec();
            }    
        }
    }
        else{
            QMessageBox mbox;
            mbox.setText(errors);
            mbox.exec();
        }
    date_Set();
}

// заполняем базу данных
void MainWindow::date_Set()
{
    QString namedir = homedir;
    QTextStream(&namedir) << tr("/.qt5-cryptopro-csp");
    QDir * mkdir = new QDir;
    const QString dir = namedir;
    if(!mkdir->mkdir(dir)){
        QTextStream(&namedir) << tr("/polname.dat");
    }else QTextStream(&namedir) << tr("/polname.dat");
    delete mkdir;

    QFile file (namedir);
    file.open(QIODevice::WriteOnly);
    QDataStream out(&file);

    map["SN"] = ui->lineEdit_SN->text();
    map["G"] = ui->lineEdit_G->text();
    map["I"] = ui->lineEdit_I->text();
    map["CN"] = ui->lineEdit_CN->text();
    map["OID.1.2.840.113549.1.9.2"] = ui->lineEdit_UN->text();
    map["T"] = ui->lineEdit_T->text();
    map["OU"] = ui->lineEdit_OU->text();
    map["O"] = ui->lineEdit_O->text();
    map["STREET"] = ui->lineEdit_Street->text();
    map["E"] = ui->lineEdit_E->text();
    map["SNILS"] = ui->lineEdit_Snils->text();
    map["INN"] = ui->lineEdit_INN->text();
    map["OGRN"] = ui->lineEdit_OGRN->text();
    map["L"] = ui->lineEdit_L->text();
    map["passportser"] = ui->lineEdit_pasp_ser->text();
    map["passport"] = ui->lineEdit_pasport->text();
    map["data"] = ui->lineEdit_data->text();
    map["issued"] = ui->lineEdit_issued->text();
    map["authorized"] = ui->lineEdit_authorized->text();
    map["authorfull"] = ui->lineEdit_author_full->text();
    map["authorT"] = ui->lineEdit_authorized_r->text();
    map["authorPs"] = ui->lineEdit_pasp_ser_aut->text();
    map["authorP"] = ui->lineEdit_paspaut->text();
    map["authorD"] = ui->lineEdit_pasdata->text();
    map["authorIs"] = ui->lineEdit_pasissued->text();
    map["post"] = ui->lineEdit_post->text();
    map["boss"] = ui->lineEdit_boss->text();

    out << map;

    file.close();
}

// заполняем форму данными из базы
void MainWindow::data_Read()
{
    QString namedir = QDir::homePath();
    QTextStream(&namedir) << "/.qt5-cryptopro-csp/polname.dat";
    QFile file (namedir);
    file.open(QIODevice::ReadOnly);
    QDataStream in(&file);

    in >> map;

    ui->lineEdit_SN->setText(map.value("SN"));
    ui->lineEdit_G->setText(map.value("G"));
    ui->lineEdit_I->setText(map.value("I"));
    ui->lineEdit_UN->setText(map.value("OID.1.2.840.113549.1.9.2"));
    ui->lineEdit_T->setText(map.value("T"));
    ui->lineEdit_OU->setText(map.value("OU"));
    ui->lineEdit_O->setText(map.value("O"));
    ui->lineEdit_Street->setText(map.value("STREET"));
    ui->lineEdit_E->setText(map.value("E"));
    ui->lineEdit_Snils->setText(map.value("SNILS"));
    ui->lineEdit_INN->setText(map.value("INN"));
    ui->lineEdit_OGRN->setText(map.value("OGRN"));
    ui->lineEdit_L->setText(map.value("L"));
    ui->lineEdit_pasp_ser->setText(map.value("passportser"));
    ui->lineEdit_pasport->setText(map.value("passport"));
    ui->lineEdit_data->setText(map.value("data"));
    ui->lineEdit_issued->setText(map.value("issued"));
    ui->lineEdit_authorized->setText(map.value("authorized"));
    ui->lineEdit_author_full->setText(map.value("authorfull"));
    ui->lineEdit_authorized_r->setText(map.value("authorT"));
    ui->lineEdit_pasp_ser_aut->setText(map.value("authorPs"));
    ui->lineEdit_paspaut->setText(map.value("authorP"));
    ui->lineEdit_pasdata->setText(map.value("authorD"));
    ui->lineEdit_pasissued->setText(map.value("authorIs"));
    ui->lineEdit_post->setText(map.value("post"));
    ui->lineEdit_boss->setText(map.value("boss"));

    file.close();
}


// Функция проверки установки КриптоПро
bool MainWindow::TestCryptopro(){
    QString crypt = cryptPath;
    QTextStream(&crypt) << tr("cryptcp");
    QFile cryptopro(crypt);
    return cryptopro.exists();
}

//Слоты

//заполнения CN
void MainWindow::on_lineEdit_SN_textChanged(const QString &arg1)
{
    QString name = arg1;
    QTextStream(&name) << " " << ui->lineEdit_G->text();
    ui->lineEdit_CN->setText(name);
}

void MainWindow::on_lineEdit_G_textChanged(const QString &arg1)
{
    QString sn = ui->lineEdit_SN->text();
    QTextStream(&sn) << " " << arg1;
    ui->lineEdit_CN->setText(sn);
}

//Отключение включение checkBox выбора типа контейнеров
void MainWindow::on_checkBox_makekey_stateChanged(int arg1)
{
    ui->comboBox_fd->setDisabled(true);

    if (arg1){
        ui->comboBox_fd->setDisabled(false);
    }
}


// установкa крипто про
void MainWindow::on_pushButton_cryptopro_clicked()
{
   bool b = true;
   QMessageBox * errorbox = new QMessageBox(this);
   if(TestCryptopro()){
       errorbox->setText("\"КриптоПро CSP\" уже установлен");
       errorbox->setInformativeText("Переустановить \"КритпоПро CSP\"?");
       QAbstractButton * yes = errorbox->addButton(QMessageBox::Yes);
       errorbox->addButton(QMessageBox::No);
       errorbox->show();
       errorbox->exec();
       if (errorbox->clickedButton() == yes);
       else b = false;
   }
   if (b){
       delete errorbox;
       Instalgui * installg = new Instalgui();
       installg->show();
       installg->exec();
   }
   else delete errorbox;
}

// Установка лицензии
// cpconfig -license -set KEY

void MainWindow::on_pushButton_lic_clicked()
{
    if (ui->lineEdit_lic->text() != "")
    {
        QString cpconfigPath = "screepts/cpconfig.sh";
        QStringList arguments;
        QProcess * cpconfig = new QProcess();
        cpconfig->setProcessChannelMode(QProcess::MergedChannels);
        cpconfig->start(cpconfigPath, arguments << ui->lineEdit_lic->text());
        if (!cpconfig->waitForFinished()){
            QString ERror = cpconfig->errorString();
            QMessageBox mbox;
            if (ERror.length() != 0){
                mbox.setText(ERror);
                mbox.exec();
               }
            else {
                mbox.setText("ОК");
                QMessageBox mbox;
                mbox.setText(cpconfig->errorString());
                mbox.exec();
            }
        }
        else{
            QMessageBox mbox;
            QString ERror = cpconfig->readAllStandardOutput();
            if (ERror.length() != 0){
                mbox.setText(ERror);
                mbox.exec();
               }
            else {
                mbox.setText("Лицензия установленна");
                QMessageBox mbox;
                mbox.setText(cpconfig->errorString());
                mbox.exec();
            }
            QString cpconfigPath = scryptPath;
            QTextStream(&cpconfigPath) << tr("cpconfig");
            QString license = LicenseVerific(cpconfigPath);
            if (license.length() != 0){
                ui->label_cryptopro->setText(license);
            }
        }
        delete cpconfig;
    }
    else{
        QMessageBox mbox;
        mbox.setText("Пусто! Введите серийный номер\n");
        mbox.exec();
    }
}

//Функция проверки Лицензии

QString MainWindow::LicenseVerific(QString cpconfigPath){
    QString crypt = "";
    QStringList arguments;
    QProcess * cpconfig = new QProcess();
    cpconfig->setProcessChannelMode(QProcess::MergedChannels);
    cpconfig->start(cpconfigPath, arguments << "-license" << "-view");
    if (!cpconfig->waitForFinished()){
        QMessageBox mbox;
        mbox.setText(cpconfig->errorString());
        mbox.exec();
    }
    else{
        QTextStream(&crypt) << tr(cpconfig->readAllStandardOutput());
    }
    delete cpconfig;
    return crypt;
}

// Поиск контейнера
// csptest -keyset -enum_cont -fqcn -verifyc

void MainWindow::on_pushButton_keys_clicked()
{
    QString cryptPathnew = cryptPath;
    QStringList arguments;
    QProcess csptest;
    csptest.setProcessChannelMode(QProcess::MergedChannels);
    csptest.start(cryptPathnew.append("csptest"), arguments << "-keyset" << "-enum_cont" << "-fqcn" << "-verifyc");
    if (!csptest.waitForFinished()){
        QMessageBox mbox;
        mbox.setText(csptest.errorString());
        mbox.exec();
        csptest.close();
    }
    else{
        QString out;
        out.append(csptest.readAllStandardOutput());
        QStringList keys = out.split("\n");
        ui->comboBox_seakeys->clear();
        foreach (QString k, keys) {
           if (k.startsWith("\\")){
               ui->comboBox_seakeys->addItem(k);
           }
            csptest.close();
        }
        ui->comboBox_seakeys->addItem("none");
        keys.clear();
        csptest.close();
    }
    arguments.clear();
}

// Установка корневого сертификата
// certmgr -inst -file certnew.p7b -store uRoot

void MainWindow::on_pushButton_rootkey_clicked()
{
    QString cryptPathnew = cryptPath;
    QStringList arguments;
    QFileDialog * rtdialog = new QFileDialog();
    QString namef = rtdialog->getOpenFileName(this,"Выберите корневой сертификат удостоверяющего центра",homedir,"*.p7b *.cer *.crt");

    delete rtdialog;

    QProcess certmgr;
    certmgr.setProcessChannelMode(QProcess::MergedChannels);
    if (namef.contains(".p7b", Qt::CaseInsensitive)){
        certmgr.start(cryptPathnew.append("certmgr"), arguments << "-inst" << "-file" << namef << "-all" << "-store" << "uRoot");
        if (!certmgr.waitForFinished()){
            QMessageBox mbox;
            mbox.setText(certmgr.errorString());
            mbox.exec();
            certmgr.close();
        }
        else{
            QString out;
            QMessageBox mbox;
            out.append(certmgr.readAllStandardOutput());
            mbox.setText(out);
            mbox.exec();
            certmgr.close();
        }
    }
    else if (namef.contains(".cer", Qt::CaseInsensitive) || namef.contains(".crt", Qt::CaseInsensitive)) {
        certmgr.start(cryptPathnew.append("certmgr"), arguments << "-inst" << "-file" << namef << "-store" << "uRoot");
        if (!certmgr.waitForFinished()){
            QMessageBox mbox;
            mbox.setText(certmgr.errorString());
            mbox.exec();
            certmgr.close();
        }
        else{
            QString out;
            QMessageBox mbox;
            out.append(certmgr.readAllStandardOutput());
            mbox.setText(out);
            mbox.exec();
            certmgr.close();
        }
    }
    else{
        QMessageBox mbox;
        mbox.setText("Файл сертификата не выбран");
        mbox.exec();
    }
}

//Установка списка отозванных сертификатов
// certmgr -inst -crl -file

void MainWindow::on_pushButton_CRL_clicked()
{
    QString cryptPathnew = cryptPath;
    QStringList arguments;
    QFileDialog * rtdialog = new QFileDialog();
    QString namef = rtdialog->getOpenFileName(this,"Выберите файл списка отозванных сертификатов",homedir,"*.crl");
    delete rtdialog;

    if (namef.contains(".crl", Qt::CaseInsensitive)){
        QProcess certmgr;
        certmgr.setProcessChannelMode(QProcess::MergedChannels);
        certmgr.start(cryptPathnew.append("certmgr"), arguments << "-inst" << "-crl" << "-file" << namef << "-store" << "mCA");
        if (!certmgr.waitForFinished()){
            QMessageBox mbox;
            mbox.setText(certmgr.errorString());
            mbox.exec();
            certmgr.close();
        }
        else{
            QString out;
            QMessageBox mbox;
            out.append(certmgr.readAllStandardOutput());
            out.append(certmgr.readAllStandardOutput());
            mbox.setText(out);
            mbox.exec();
            certmgr.close();
            }
    }
    else {
        QMessageBox mbox;
        mbox.setText("Файл списка отозванных сертификатов не выбран");
        mbox.exec();
    }
}

// Посмотреть сертификат в контейнере
void MainWindow::on_pushButton_seakeys_clicked()
{
    if (ui->comboBox_seakeys->currentText() != "none")
    {
        QString pathmgr = cryptPath;
        QTextStream (&pathmgr) << "certmgr";
        Dialog * certdialog = new Dialog();
        certdialog->pathcr = pathmgr;
        certdialog->cont =  ui->comboBox_seakeys->currentText();
        certdialog->checked = ui->checkBox_tipe->isChecked();
        certdialog->labelText();
        certdialog->show();
        certdialog->exec();
    }
    else {
        QMessageBox mbox;
        mbox.setText("Не выбран контейнер");
        mbox.exec();
    }

}

//Устанвка сертификата пользователя
//certmgr -inst -file certnew.p7b -store uMy -cont '\\.\HDIMAGE\test' -inst_to_cont

void MainWindow::on_pushButton_usercer_clicked()
{
    QString namecont = ui->comboBox_seakeys->currentText();
    if (namecont != "none")
    {
        QString cryptPathnew = cryptPath;
        QFileDialog * rtdialog = new QFileDialog();
        QString namef = rtdialog->getOpenFileName(this,"Выберите файл пользователя",homedir,"*.p7b *.cer *.crt");

        delete rtdialog;

        if (namef.contains(".p7b", Qt::CaseInsensitive) || namef.contains(".cer", Qt::CaseInsensitive) || namef.contains(".crt", Qt::CaseInsensitive))
        {
            QProcess certmgr;
            QTextStream(&cryptPathnew) << "certmgr -inst -file \"" << namef << "\" -store uMy " << "-cont "
                                       << namecont;
            certmgr.setProcessChannelMode(QProcess::MergedChannels);
            certmgr.start(cryptPathnew);
            if (!certmgr.waitForFinished()){
                QMessageBox mbox;
                mbox.setText(certmgr.errorString());
                mbox.exec();
                certmgr.close();
            }
            else{
                QString out;
                QMessageBox mbox;
                out.append(certmgr.readAllStandardOutput());
                out.append(certmgr.readAllStandardOutput());
                mbox.setText(out);
                mbox.exec();
                certmgr.close();
                }
        certmgr.close();
        }
        else {
            QMessageBox mbox;
            mbox.setText("Файл не выбран");
            mbox.exec();
        }
    }
    else {
        QMessageBox mbox;
        mbox.setText("Не выбран контейнер");
        mbox.exec();
    }
}

//certmgr -list
//простмотр установленных сертификатов пользователя

void MainWindow::on_pushButton_sysser_clicked()
{
    QString cryptPathnew = cryptPath;
    QProcess certmgr;
    QTextStream(&cryptPathnew) << "certmgr -list";
    certmgr.setProcessChannelMode(QProcess::MergedChannels);
    certmgr.start(cryptPathnew);
    if (!certmgr.waitForFinished()){
        QMessageBox mbox;
        mbox.setText(certmgr.errorString());
        mbox.exec();
        certmgr.close();
    }
    else{
        QString out;
        QMessageBox mbox;
        out.append(certmgr.readAllStandardOutput());
        out.append(certmgr.readAllStandardOutput());
        mbox.setText(out);
        mbox.exec();
        certmgr.close();
        }
    certmgr.close();
}

//Удаление контейнеров ключей
void MainWindow::on_pushButton_delkey_clicked()
{
    if (ui->comboBox_seakeys->currentText() != "none")
    {
        QString cryptPathnew = cryptPath;
        QProcess certmgr;
        QTextStream(&cryptPathnew) << "csptest -keyset -deletekeyset -cont " <<  ui->comboBox_seakeys->currentText();
        certmgr.setProcessChannelMode(QProcess::MergedChannels);
        certmgr.start(cryptPathnew);
        if (!certmgr.waitForFinished()){
            QMessageBox mbox;
            mbox.setText(certmgr.errorString());
            mbox.exec();
            certmgr.close();
        }
        else{
            QString out;
            QMessageBox mbox;
            out.append(certmgr.readAllStandardOutput());
            out.append(certmgr.readAllStandardOutput());
            mbox.setText(out);
            mbox.exec();
            certmgr.close();
            }
        certmgr.close();
    }
    else {
        QMessageBox mbox;
        mbox.setText("Не выбран контейнер");
        mbox.exec();
    }
}

//Удалить все сертификаты
void MainWindow::on_pushButton_delcer_clicked()
{
    QString cryptPathnew = cryptPath;
    QProcess certmgr;
    QTextStream(&cryptPathnew) << "certmgr -delete -all";
    certmgr.setProcessChannelMode(QProcess::MergedChannels);
    certmgr.start(cryptPathnew);
    if (!certmgr.waitForFinished()){
        QMessageBox mbox;
        mbox.setText(certmgr.errorString());
        mbox.exec();
        certmgr.close();
    }
    else{
        QString out;
        QMessageBox mbox;
        out.append(certmgr.readAllStandardOutput());
        out.append(certmgr.readAllStandardOutput());
        mbox.setText(out);
        mbox.exec();
        certmgr.close();
        }
    certmgr.close();
}

//Установить сертификат пользователя из контейнера
void MainWindow::on_pushButton_cerkeys_clicked()
{
    if (ui->comboBox_seakeys->currentText() != "none")
    {
        QProcess certmgr;
        QString conteiner;
        if (ui->checkBox_tipe->isChecked()){
            QTextStream (&conteiner) << cryptPath << "certmgr" << " -inst" << " -cont " << ui->comboBox_seakeys->currentText() << " -store uMy";
        }
        else{
            QTextStream (&conteiner) << cryptPath << "certmgr" << " -inst" << " -ask-cont " << "-at_signature" << " -cont " << ui->comboBox_seakeys->currentText() << " -store uMy";
        }

        certmgr.setProcessChannelMode(QProcess::MergedChannels);
        certmgr.start(conteiner);
        if (!certmgr.waitForFinished()){
            QMessageBox mbox;
            mbox.setText(certmgr.errorString());
            mbox.exec();
            certmgr.close();
        }
        else{
            QString out;
            QMessageBox mbox;
            out.append(certmgr.readAllStandardOutput());
            out.append(certmgr.readAllStandardOutput());
            mbox.setText(out);
            mbox.exec();
            certmgr.close();
            }
        certmgr.close();
    }
    else {
        QMessageBox mbox;
        mbox.setText("Не выбран контейнер");
        mbox.exec();
    }
}

//Генерация документа заявки на сертификат
void MainWindow::on_pushButton_apply_clicked()
{
    date_Set();
    QString n;
    QString c = ui->lineEdit_CN->text();
    mytranslit translit;
    translit.myTranslit(c, n);

    QFileDialog * cdialog = new QFileDialog();
    QString namedir = cdialog->getExistingDirectory(this,"Выберите каталог для сохранения файла заявки",homedir);
    delete cdialog;
    if(namedir.length() != 0){
        QString namefile = namedir;
        QTextStream(&namefile) << "/" << n << ".odt";
        applyDoc * apply = new applyDoc();
        apply->shapeapply();
        apply->write(namefile);
        QMessageBox mbox;
        QString message = "Фаил заявки создан в:\n";
        QTextStream(&message) << "  " << namefile << "\n";
        mbox.setText(message);
        mbox.exec();
        delete apply;
    }
    else return;
}

//Генерация документа доверенности
void MainWindow::on_pushButton_trast_clicked()
{
    date_Set();
    QString n;
    QString c = ui->lineEdit_CN->text();

    mytranslit translit;
    translit.myTranslit(c, n);

    QFileDialog * cdialog = new QFileDialog() ;
    QString namedir = cdialog->getExistingDirectory(this,"Выберите каталог для сохранения файла доверенности",homedir);
    delete cdialog;

    if(namedir.length() != 0){
        QString namefile = namedir;
        QTextStream(&namefile) << "/" << "trast_" << n << ".odt";

        applyDoc * apply = new applyDoc();
        apply->shapetrast();
        apply->write(namefile);
        QMessageBox mbox;
        QString message = "Фаил доверенности создан в:\n";
        QTextStream(&message) << "  " << namefile << "\n";
        mbox.setText(message);
        mbox.exec();
        delete apply;
    }
    else return;
}

// Подписание, шифрование файлов
// cryptcp -sign  -dn "Test User5" -der /home/guest/Ivanov_Ivan_Ivanovich.odt

void MainWindow::on_pushButton_crypt_clicked()
{
    QProcess cryptcp;
    QString cryptPathnew = cryptPath;

    if (ui->checkBox_crypt->isChecked()){
        QFileDialog * fdialog = new QFileDialog();
        QString namefile = fdialog->getOpenFileName(this,"Выберите файл для подписания",homedir);
        delete fdialog;
        if (namefile.length() != 0){
            QTextStream(&cryptPathnew) << "cryptcp -signf  -dn " << "\"" << ui->lineEdit_serfindcn->text()
                                       << "\"" << " -der " << "\"" << namefile << "\"";

            cryptcp.setProcessChannelMode(QProcess::MergedChannels);
            cryptcp.start(cryptPathnew);
            if (!cryptcp.waitForFinished()){
                QMessageBox mbox;
                mbox.setText(cryptcp.errorString());
                mbox.exec();
                cryptcp.close();
            }
            else{
                QString out;
                QMessageBox mbox;
                out.append(cryptcp.readAllStandardOutput());
                out.append(cryptcp.readAllStandardOutput());
                mbox.setText(out);
                mbox.exec();
                cryptcp.close();
                }
            cryptcp.close();
        }
    }
    else{
        QFileDialog * fdialog = new QFileDialog();
        QString namefile = fdialog->getOpenFileName(this,"Выберите файл для подписания и шифрования",homedir);
        delete fdialog;
        if(namefile.length() != 0){
            QTextStream(&cryptPathnew) << "/cryptcp -sign  -dn " << "\"" << ui->lineEdit_serfindcn->text()
                                       << "\"" << " -der " << namefile;
            cryptcp.setProcessChannelMode(QProcess::MergedChannels);
            cryptcp.start(cryptPathnew);
            if (!cryptcp.waitForFinished()){
                QMessageBox mbox;
                mbox.setText(cryptcp.errorString());
                mbox.exec();
                cryptcp.close();
            }
            else{
                QString out;
                QMessageBox mbox;
                out.append(cryptcp.readAllStandardOutput());
                out.append(cryptcp.readAllStandardOutput());
                mbox.setText(out);
                mbox.exec();
                cryptcp.close();
                }
            cryptcp.close();
        }
    }
}

// Проверка подписи файла
// /opt/cprocsp/bin/<amd64 или ia32>/cryptcp -verify <указать_папку_файла/название_файла>

void MainWindow::on_pushButton_verifile_clicked()
{
    QFileDialog * fdialog = new QFileDialog();
    QString namefile = fdialog->getOpenFileName(this,"Выберите файл для проверки подписи",homedir);
    delete fdialog;

    QProcess cryptcp;
    QString cryptPathnew = cryptPath;

    if(namefile.length() != 0){
        QTextStream(&cryptPathnew) << "cryptcp -verify " << "\"" << namefile << "\"";
        cryptcp.setProcessChannelMode(QProcess::MergedChannels);
        cryptcp.start(cryptPathnew);
        if (!cryptcp.waitForFinished()){
            QMessageBox mbox;
            mbox.setText(cryptcp.errorString());
            mbox.exec();
            cryptcp.close();
        }
        else{
            QString out;
            QMessageBox mbox;
            out.append(cryptcp.readAllStandardOutput());
            out.append(cryptcp.readAllStandardOutput());
            mbox.setText(out);
            mbox.exec();
            cryptcp.close();
            }
        cryptcp.close();
    }
}

