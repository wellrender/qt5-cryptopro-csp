#ifndef APPLYDOC_H
#define APPLYDOC_H
#include <QtCore/QMap>
#include <QTextDocument>
#include <QTextCursor>

class applyDoc
{
public:
    applyDoc();
    ~applyDoc();

    void shapeapply();
    void shapetrast();

    void write(const QString &fileName);

private:
      QTextDocument * const m_document;
      QTextCursor m_cursor;
      QMap<QString, QString> map;
};

#endif // APPLYDOC_H
