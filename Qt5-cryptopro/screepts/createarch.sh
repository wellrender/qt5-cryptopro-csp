#!/usr/bin/env bash

if [! -d "~/buildcpcsp"]
then
    mkdir ~/buildcpcsp
fi
tar -xvzf $1 -C ~/buildcpcsp/
