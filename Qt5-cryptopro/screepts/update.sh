#!/usr/bin/env bash
echo "start" > /tmp/start.pid
echo "Синхронизация репозитория" > /tmp/log.txt
if [!`gksu "apt-get update >> /tmp/log.txt"`]
then
    echo "Ошибка синхронизации" > /tmp/error.txt
fi
echo "END1" >> /tmp/log.txt
rm -f /tmp/start.pid
