#!/usr/bin/env bash
echo "start" >> /tmp/start.pid
if [!`gksu "apt-get install cryptopro-preinstall-full >> /tmp/log.txt"`]
then
    echo "Ошибка установки зависимостей" > /tmp/error.log
fi
echo "END2" >> /tmp/log.txt
rm -f /tmp/start.pid
