#!/usr/bin/env bash
echo "start" >> /tmp/start.pid
cd ~/buildcpcsp/linux-ia32
if [!`gksu "apt-get install cprocsp-rdr-gui-gtk* cprocsp-rdr-pcsc* lsb-cprocsp-pkcs11* >> /tmp/log.txt"`]
then
    echo "Ошибка установки дополнительных пакетов" > /tmp/error.log
fi
echo "END4" >> /tmp/log.txt
rm -f /tmp/start.pid
