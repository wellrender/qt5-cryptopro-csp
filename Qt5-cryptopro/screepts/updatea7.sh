#!/usr/bin/env bash
echo "start" >> /tmp/start.pid
if [!`gksu "apt-get install cryptopro-preinstall >> /tmp/log.txt"`]
then
    echo "Ошибка установки дополнительных пакетов" > /tmp/error.log
fi
echo "END2" >> /tmp/log.txt
rm -f /tmp/start.pid
