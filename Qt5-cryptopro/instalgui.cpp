#include "instalgui.h"
#include "ui_instalgui.h"

#include <QFileDialog>
#include <QtCore/QProcess>
#include <QtCore/QTextStream>
#include <QThread>

Instalgui::Instalgui(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Instalgui)
{
    ui->setupUi(this);
}

Instalgui::~Instalgui()
{
    delete ui;
}

void Instalgui::on_pushButton_2_clicked()
{
    close();
}

void Instalgui::on_pushButton_scp_clicked()
{
    QString namearch;
    if (ui->comboBox_OS->currentIndex() == 0){
        namearch = "linux-ia32_deb.tgz";
    }else namearch = "linux-ia32.tgz";
    QString homename = QDir::homePath();

    QFileDialog * filedialog = new QFileDialog();
    QString cryptcsp = filedialog->getOpenFileName(this,"Выберите фаил архива КриптоПро CSP",homename, namearch);
    if (cryptcsp.length() != 0){
        QStringList arguments;
        QProcess * extract = new QProcess();
        extract->setProcessChannelMode(QProcess::MergedChannels);
        extract->start("screepts/createarch.sh", arguments << cryptcsp);
        if (!extract->waitForFinished(50000)){
            ui->textEdit_install->append(extract->errorString());
            delete extract;
        }
        else{
            QString out;
            QTextStream (&out) << extract->readAllStandardOutput() << tr("\nРаспаковка архива завершена\n");
            ui->textEdit_install->append(out);
            ui->progressBar->setValue(2);
            ui->pushButton_scp->setDisabled(true);
            delete extract;
        }
    }
    else{
        ui->textEdit_install->append("Фаил архива не выбран\n");
    }
}

void Instalgui::on_pushButton_cades_clicked()
{
    QString homename = QDir::homePath();
    QFileDialog * filedialog = new QFileDialog();
    QString cadesplugin = filedialog->getOpenFileName(this,"Выберите фаил архива плагина",homename,"cades_linux_ia32.tar.gz");
    if (cadesplugin.length() != 0){
        QStringList arguments;
        QProcess * extract = new QProcess();
        extract->setProcessChannelMode(QProcess::MergedChannels);
        extract->start("screepts/createarch.sh", arguments << cadesplugin);
        if (!extract->waitForFinished(50000)){
            ui->textEdit_install->append(extract->errorString());
            delete extract;
        }
        else{
            QString out;
            QTextStream (&out) << extract->readAllStandardOutput() << tr("\nРаспаковка архива завершена\n");
            ui->textEdit_install->append(out);
            ui->progressBar->setValue(4);
            ui->pushButton_cades->setDisabled(true);
            delete extract;
        }
    }
    else{
        ui->textEdit_install->append("Фаил архива не выбран\n");
    }
}

// слот инсталяции кипто про
void Instalgui::on_pushButton_clicked()
{
    QFile fileout ("/tmp/install.log");
    fileout.open(QIODevice::WriteOnly);
    QTextStream outf(&fileout);

    int Value = 4;
    QString updatepath;
    QStringList arguments;
    int system = ui->comboBox_OS->currentIndex();
    QString homename = QDir::homePath();
    if (system == 0){
        QTextStream (&homename) << tr("/buildcpcsp/linux-ia32_deb/install.sh");
    }
    else QTextStream(&homename) << tr("/buildcpcsp/linux-ia32/install.sh");
    QFile file(homename);
    if (!file.exists()){
        ui->textEdit_install->append("Нет файла установки\n");
    }
    else{
        ui->textEdit_install->append("Синхронизация репозитория\n");
        updatepath = "screepts/update.sh";
        arguments.clear();
        QFile pidfile ("/tmp/start.pid");
        QFile errorfile ("/tmp/error.log");
        QProcess * Install = new QProcess;
        Install->setProcessChannelMode(QProcess::MergedChannels);
        if(Install->startDetached(updatepath, arguments)){ 
            QThread::msleep(50);
            while (pidfile.exists()){
               QThread::sleep(3);
               Value++;
               ui->progressBar->setValue(Value);
               if(errorfile.exists()){
                   return;
               }
            }
            ui->textEdit_install->append("Завершено");
        }
        else{
            ui->textEdit_install->append("Ошибка синхронизации\n");
            outf << ui->textEdit_install->toPlainText();
            fileout.close();
            return;
        }

        ui->textEdit_install->append("Установка необходимых зависимостей\n");
        if (system == 0){
            updatepath = "screepts/updated78.sh";
        }
        else if (system == 1){
            updatepath = "screepts/updatea7.sh";
        }
        else{
            updatepath = "screepts/updatea8.sh";
        }
        if(Install->startDetached(updatepath, arguments)){
            QThread::msleep(50);
            while (pidfile.exists()){
               QThread::sleep(2);
               Value++;
               ui->progressBar->setValue(Value);
               if(errorfile.exists()){
                   return;
               }
            }
        ui->textEdit_install->append("Завершено\n");
        }
        else{
            ui->textEdit_install->append("Ошибка Установки необходимых зависимостей\n");
            outf << ui->textEdit_install->toPlainText();
            fileout.close();
            return;
        }

        delete Install;
        ui->textEdit_install->append("Установка базовых пакетов КриптоПро\n");
        arguments.clear();
        updatepath = "screepts/installcpcsp.sh";
        QFile installsh (homename);
        if(!installsh.exists()){
            ui->textEdit_install->append("Ошибка установки базовых пакетов\nФаил установки не доступен.\n");
            return;
        }
        if(Install->startDetached(updatepath, arguments << homename)){
            QThread::msleep(50);
            while (pidfile.exists()){
               QThread::sleep(1);
               Value++;
               ui->progressBar->setValue(Value);
               if(errorfile.exists()){
                   return;
               }
            }
        ui->textEdit_install->append("Завершено\n");
        }
        else{
            ui->textEdit_install->append("Ошибка установки базовых пакетов\n");
            return;
        }

        Value = 60;
        ui->textEdit_install->append("Установка Завершена!\n");
        ui->progressBar->setValue(Value);

        ui->textEdit_install->append("Установка дополнительных пакетов КриптоПро\n");
        arguments.clear();

        if (system == 0){
            updatepath = "screepts/installpak.sh";
        }
        else{
            updatepath = "screepts/installpaka.sh";
        }

        if(Install->startDetached(updatepath)){
            QThread::msleep(50);
            while (pidfile.exists()){
               QThread::sleep(1);
               Value++;
               ui->progressBar->setValue(Value);
               if(errorfile.exists()){
                   return;
               }
            }
            ui->textEdit_install->append("Завершено");
        }
        else{
            ui->textEdit_install->append("Ошибка Установки дополнительных пакетов\n");
            outf << ui->textEdit_install->toPlainText();
            fileout.close();
            return;
        }

        ui->textEdit_install->append("Установка плагина КриптоПро\n");
        arguments.clear();

        if(system == 0){
            updatepath = "screepts/installcades.sh";
        }
        else{
            updatepath = "screepts/installcadesa.sh";
        }

        if(Install->startDetached(updatepath)){
            QThread::msleep(50);
            while (pidfile.exists()){
               if(system == 0)
                    QThread::sleep(4);
               else QThread::sleep(2);
               Value++;
               ui->progressBar->setValue(Value);
               if(errorfile.exists()){
                   return;
               }
            }
            while(Value < 100){
                Value++;
                ui->progressBar->setValue(Value);
            }
            ui->textEdit_install->append("Завершено");
        }
        else{
            ui->textEdit_install->append("Ошибка Установки плагина\n");
            outf << ui->textEdit_install->toPlainText();
            fileout.close();
            return;
        }
        outf << ui->textEdit_install->toPlainText();
        fileout.close();
    }
}
