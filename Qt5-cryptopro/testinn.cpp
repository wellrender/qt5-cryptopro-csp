#include "testinn.h"

// реализация класса проверки ИНН и SNILS
testinn::testinn(){
    QString inn;
}

testinn::~testinn(){

}

// функция проверки инн по алгоритму  https://www.egrul.ru/test_inn.html
bool testinn::testinns(const QString &inn){
    if (inn.startsWith("00")){
        QString inn10 = inn;
        inn10.remove(0,2);
        unsigned int numbers[10];
        unsigned int k[10] = {2,4,10,3,5,9,4,6,8,0};
        unsigned int sum = 0;
        unsigned int ks;
        for (int i = 0; i < 10; ++i) {
            numbers[i] = inn10[i].digitValue();
        }
        for (int n = 0; n < 10; ++n){
            sum += (numbers[n] * k[n]);
        }
        ks = sum % 11;
        if (ks > 9){
            ks %= 10;
        }
        if (ks == numbers[9])
            return true;
        else return false;
    }else{
        QString inn12 = inn;
        unsigned int numbers[12];
        unsigned int k1[11] = {7,2,4,10,3,5,9,4,6,8,0};
        unsigned int k2[12] = {3,7,2,4,10,3,5,9,4,6,8,0};
        unsigned int sum = 0;
        unsigned int ks1;
        unsigned int ks2;
        for (int i = 0; i < 12; ++i) {
            numbers[i] = inn12[i].digitValue();
        }
        for (int n = 0; n < 11; ++n){
            sum += numbers[n] * k1[n];
        }
        ks1 = sum % 11;
        if (ks1 > 9){
            ks1 %= 10;
        }
        sum = 0;
        for (int n = 0; n < 12; ++n){
            sum += numbers[n] * k2[n];
        }
        ks2 = sum % 11;
        if (ks2 > 9){
            ks2 %= 10;
        }
        if (ks1 == numbers[10] && ks2 == numbers[11])
            return true;
        else
            return false;
    }
}

// проверка снилс по алгоритму

bool testinn::testsnils(const QString &inn){
    QString snils = inn;
    unsigned short n = 9;
    unsigned short numbers[11];
    unsigned int sum = 0;

    for (short i = 0; i < 11; ++i){
        numbers[i] = snils[i].digitValue();
    }

    for (short i = 0; i < 9; ++i){
        sum += numbers[i] * n;
        n--;
    }

    if (sum > 101){
        sum %= 101;
    }

    if (sum == 100 || sum == 101){
        sum = 0;
    }
    unsigned short kn = numbers[9] * 10 + numbers[10];

    if(kn == sum){
        return true;
    }else return false;

}
