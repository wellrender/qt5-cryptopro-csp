#ifndef INSTALGUI_H
#define INSTALGUI_H

#include <QDialog>

namespace Ui {
class Instalgui;
}

class Instalgui : public QDialog
{
    Q_OBJECT

public:
    explicit Instalgui(QWidget *parent = 0);
    ~Instalgui();

private slots:
    void on_pushButton_2_clicked();

    void on_pushButton_scp_clicked();

    void on_pushButton_cades_clicked();

    void on_pushButton_clicked();
    
private:
    Ui::Instalgui *ui;
};

#endif // INSTALGUI_H
