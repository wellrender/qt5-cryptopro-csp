#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include <QAbstractButton>

namespace Ui {
class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog(QWidget *parent = 0);
    ~Dialog();
    void labelText();
    QString pathcr;
    QString cont;
    bool checked;

private slots:

    void on_pushButton_quit_clicked();

    void on_pushButton_inst_clicked();

    void on_pushButton_export_clicked();

private:
    Ui::Dialog *ui;
    QString subject;
};

#endif // DIALOG_H
